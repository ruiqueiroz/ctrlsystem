package com.spartan.code.exemplo.dto;

import lombok.Data;

@Data
public class FilterContactDTO {

   // private ContactType type;

    private String name;

    private String email;

}
