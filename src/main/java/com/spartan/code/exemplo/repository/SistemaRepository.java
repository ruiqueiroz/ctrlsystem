package com.spartan.code.exemplo.repository;

import com.spartan.code.exemplo.domain.Sistema;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SistemaRepository  extends JpaRepository<Sistema, Long> {

}
