package com.spartan.code.exemplo.service;

import java.util.List;

import com.spartan.code.exemplo.domain.Sistema;
import com.spartan.code.exemplo.repository.SistemaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

@Log
@Service
public class SistemaService {

    private final SistemaRepository repository;

    @Autowired
    public SistemaService(SistemaRepository repository) {
        this.repository = repository;
    }

    public List<Sistema> findAll() {
        log.info("Find All");
        return repository.findAll();
    }

    public Sistema findOne(Long id) {
        log.info("Find One");
        return repository.findOne(id);
    }

    public Sistema add(Sistema sistema) {
        log.info(String.format("Add: %s ", sistema.toString()));
        return repository.save(sistema);
    }

    public Sistema update(Sistema sistema) {
        log.info(String.format("Update: %s ", sistema.toString()));
        return repository.save(sistema);
    }

    public void delete(Long id) {
        log.info(String.format("Delete: %s ", id.toString()));
        repository.delete(id);
    }
}
