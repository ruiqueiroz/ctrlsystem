package com.spartan.code.exemplo.controller;


import java.util.List;

import javax.persistence.CascadeType;
import javax.validation.Valid;

import com.spartan.code.exemplo.domain.Sistema;
import com.spartan.code.exemplo.service.SistemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SistemaController {

    private final SistemaService service;

    @Autowired
    public SistemaController(SistemaService service) {
        this.service = service;
    }

    @GetMapping("/sistema")
    public List<Sistema> findAll() {
        return service.findAll();
    }

    @GetMapping(value = "/sistema/{id}")
    public Sistema findOne(@PathVariable Long id) {
        return service.findOne(id);
    }

    @PostMapping("/sistema")
    public Sistema add(@Valid @RequestBody Sistema sistema) {
        return service.add(sistema);
    }

    @PutMapping(value = "/sistema")
    public Sistema update(@RequestBody Sistema sistema) {
        return service.update(sistema);
    }

    @DeleteMapping (value = "/sistema/{id}")
    public void delete(@PathVariable(value = "id") Long id) {
        service.delete(id);
    }
}
