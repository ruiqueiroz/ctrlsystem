package com.spartan.code.exemplo.domain;

public enum NivelPrioridade {

    CRITICA, ALTA, MEDIA, BAIXA

}
